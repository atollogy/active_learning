import tensorflow as tf
from pipeline.models import Classification 
from pipeline.active_learning_base import ActiveLearningBaseClass
import os
import csv
import cv2
import numpy as np
import json

class ActiveLearningClassification(ActiveLearningBaseClass):
    def __init__(self, model_dir, batch_size=32, img_dim=(224,224), selection_strategy='QBC'):
        """
        Inititialize Active Learning for Classification object. This loads all the SavedModels

        :param model_dir : if using QBC it's a list of SavedModel directories  
        :param batch_size : batch size for inference
        :param img_dim: width and height of image 
        :param selection_strategy : Currently Classification only supports QBC
        """
        # Set batch size and img  dimensions to align with saved model generated during training
        self.batch_size = batch_size
        self.img_dim = img_dim  # width, height
        self.selection_strategy = selection_strategy
        
        if self.selection_strategy == 'QBC':
            self.M = [Classification(model_dir=model_dir_i, batch_size=batch_size, img_dim=img_dim) for model_dir_i in model_dir]
            self.n_M = len(self.M)
        elif self.selection_strategy == 'uncertainty_sampling':
            self.M = Classification(model_dir=model_dir, batch_size=batch_size, img_dim=img_dim)
        else:
            raise ValueError('{} Selection Strategy not implemented for Classification'.format(self.selection_strategy))
        print('***** Inheriting class ActiveLearningClassification from active_learning_Classification *****')
        super().__init__()

    def score(self, preds, img_name_list):
        """
        score images based on predictions

        :param preds : if using QBC preds is a list of n_committee members. Where each committee member returns a dictionary of classes: (n_imgs,) and probabilities: (n_classes, n_imgs).
        :param img_name_list : a list of files inference is run against 
        """
        img_scores = []
        if self.selection_strategy == 'QBC':
            for i, img_name_i in enumerate(img_name_list):
                # could be taking advantage of the full distribution if we used MI or Entropy
                preds_i = np.array([M_j['classes'][i] for M_j in preds])
                score_i = np.unique(preds_i).shape[0] - 1 # count disagreements
                img_scores.append([img_name_i, score_i])
        else:
            raise ValueError('{} Selection Strategy not implemented for Classification'.format(self.selection_strategy))
        return img_scores
