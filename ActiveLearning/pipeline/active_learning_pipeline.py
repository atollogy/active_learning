import json
import os
from datetime import datetime, timedelta
import logging
from sklearn.utils import shuffle

import csv
import pandas as pd
import argparse
import re
import shutil
import signal
import time
import sys

from utils.aws_helper import AWSHelperClass
from utils.misc import get_type_of_path
from pipeline.QCWB import QCWBInteract
from pipeline.object_detection import ActiveLearningObjectDetection
from pipeline.classification import ActiveLearningClassification

"""
## USAGE

1. Passing S3 location of config json
python pipeline/active_learning_pipeline --json_config_path=s3://...../config.json

2. Passing parent level folder location of S3 for a particular Active Learning model
python pipeline/active_learning_pipeline --json_config_path=s3://atl-dev--zoo/.ml/training/LP_readable_non_readable

3. Passing local file location of config
python pipeline/active_learning_pipeline --json_config_path=/Users/....../blah.json

"""


class ActiveLearningPipeline:
    """
    Class and methods for flagging images using Active Learning Query by committee
    """
    def __init__(self, json_file_path, logging_level=20):
        """
        :param json_file_path: Full path to json config file
        :param logging_level: Logger level, default INFO
        """

        # Read json file
        self.json_data = self.get_config(json_file_path)
        self.model_name = self.json_data["model"]

        # Create folders
        self.base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # <....>/imageset_builder/ActiveLearning
        self.results_dir_path = self.create_results_dir()  # .../imageset_builder/ActiveLearning/results/active_learning_pipeline_<model_name>

        self.sql_path = os.path.join(self.base_dir, 'sql_queries', 'QCWB_accurate_inaccurate.sql')

        self.logger = self.start_logger(logging_level=logging_level, logger_name='Active_learning_{}'.format(self.model_name))
        self.logger.info("### Parsed config: {}".format(json_file_path))

        self.logger.info("### Base dir : {}".format(self.base_dir))
        self.logger.info("### Results_dir:{}".format(self.results_dir_path))

        self.file_name_prefix = 'active_learning_'
        self.class_name_prefix = 'ActiveLearning'

    def start_logger(self, logging_level, logger_name):
        """
        Start logger object
        :param logging_level: 10 DEBUG, 20 INFO (Default)
        :param logger_name: Name of logger
        :return log: Logging object
        """

        log = logging.getLogger(logger_name)
        log.setLevel(int(logging_level))

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(os.path.join(self.results_dir_path, '{}.log'.format(logger_name)), mode='w+')
        fh.setLevel(int(logging_level))
        fh.setFormatter(formatter)
        log.addHandler(fh)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(int(logging_level))
        ch.setFormatter(formatter)
        log.addHandler(ch)

        log.info("### Created a logger")

        return log

    def create_results_dir(self):
        """
        Function to create results directory .../imageset_builder/ActiveLearning/results/active_learning_pipeline_<model_name>
        :return results_dir_path: Return results directory path
        """
        results_dir_name = os.path.join('results', 'active_learning_pipeline_{}'.format(self.model_name))
        results_dir_path = os.path.join(self.base_dir, results_dir_name)

        if not os.path.exists(results_dir_path):
            os.makedirs(results_dir_path)
        else:
            shutil.rmtree(results_dir_path)
            os.makedirs(results_dir_path)

        return results_dir_path

    def create_dirs(self, path):
        """
        Function to create new folders
        :param path: Full of path of folder to be created
        :return:
        """
        if not os.path.exists(path):
            os.makedirs(path)
            self.logger.info('### Created {}'.format(path))

    def get_config(self, json_path):
        """
        Function to read json file
        :param json_path: Full path to json file
        :return json_data: Data read from json file
        """
        json_data = self.read_json(json_path)
        self.model_type = json_data['type']
        self.selection_strategy = json_data['pipeline']['selection_strategy']
        print(json_data)
        return json_data

    def read_json(self, json_path):
        """
        Function to read json file
        :param json_path: full path to json file
        :return json_data: json data from json file
        """
        with open(json_path) as f:
            json_data = json.load(f)
        return json_data

    def get_start_end_date(self, date_start, date_end):
        """
        Function to get start date and end date for QB Work bench queries
        :param date_start:  start date YYYY-MM-DD
        :param date_end: end date YYYY-MM-DD
        :return start_date, end_date: datetime objects date_start-00:01:01 to date_end-23:59:59
        """

        year, month, date_str = date_start.split('-')
        start_date = datetime(int(year), int(month), int(date_str), 00, 1, 1)

        year, month, date_str = date_end.split('-')
        end_date = datetime(int(year), int(month), int(date_str), 23, 59, 59)

        return start_date, end_date


    def download_models_for_AL(self, version, s3_parent_upload_path, model_name):
        """
        Function to download model committee. If QBC is not in use the model_committee folder should only have a single directory of SavedModels
        :param version: version of model committee files to be downloaded
        :param s3_parent_upload_path: Parent S3 path to where model specific files are stored:
        s3_parent_upload_path -> model -> version -> model_committee
        :param model_name: Name of model
        :return:
        """

        model_exports = []
        train_json_path = []

        # Model committee path : s3_parent_upload_path -> model -> version -> model_committee
        s3_model_committee_path = os.path.join(s3_parent_upload_path, model_name, version, "model_committee")
        s3_bucket, s3_key = AWSHelperClass.extract_bucket_and_key(s3_model_committee_path)
        env = s3_bucket.split('-')[1]

        s3_obj = AWSHelperClass.create_s3_obj(env)

        # Local model committee path : .../imageset_builder/ActiveLearning/results/active_learning_pipeline_<model_name>/model_committee_<versionstr>
        model_committee_local_path = os.path.join(self.results_dir_path, "model_committee_{}".format(version))

        model_committee_no = 0

        # Download all models in committee and form a list to each committee folder
        while True:
            local_path = os.path.join(model_committee_local_path, str(model_committee_no))

            s3_model_key = os.path.join(s3_key, str(model_committee_no))
            res = AWSHelperClass.download_folder_from_s3(s3_obj=s3_obj, s3_bucket=s3_bucket,
                                                   s3_folder_key=s3_model_key, dest_folder_path=local_path,
                                           save_structure=False)
            if not res:
                break

            model_exports.append(local_path)
            model_committee_no +=1

        # When downloading, the saved model structure is lost. Re-building it
        for i, fldr_path in enumerate(model_exports):
            files = os.listdir(fldr_path)

            saved_model_path = os.path.join(fldr_path, 'saved_model')
            os.makedirs(saved_model_path)

            # Moving .pb file to "saved_model" folder
            pb_file = [x for x in files if x.endswith('.pb')]
            src = os.path.join(fldr_path, pb_file[0])
            dst = os.path.join(saved_model_path, pb_file[0])
            os.rename(src, dst)

            # Moving variables file to a subfolder under saved_model i.e /saved_model/variables
            variables_fldr_path = os.path.join(saved_model_path, "variables")
            os.makedirs(variables_fldr_path)
            vars_file = [x for x in files if 'variables' in x]

            for var in vars_file:
                src = os.path.join(fldr_path, var)
                dst = os.path.join(variables_fldr_path, var)
                os.rename(src, dst)

            # Getting the training params json file
            json_file = [x for x in files if 'training_params' in x][0]
            json_train_file_path = os.path.join(fldr_path, json_file)
            train_json_path.append(json_train_file_path)

        model_exports = [os.path.join(x, 'saved_model') for x in model_exports]

        # train_json_path contains path to training params json file. This file contains parameters passed during training.
        # These should be used during inference
        self.logger.info("### Models for AL :{}, Train_json_path : {} ".format(model_exports, train_json_path))
        return model_exports, train_json_path

    def run(self):
        """
        MAIN function
        :return:
        """
        train_json_path = []
        version = self.json_data["version"]
        _version_no = int(re.findall(r'\d+', version)[0])

        # If committee files path are not explicitly passed in config json, download the previous version committee files
        if not self.json_data["pipeline"]["model_committee_files_path"]:
            # Get model_exports fron this version files
            model_exports, train_json_path = self.download_models_for_AL(version=version, s3_parent_upload_path=self.json_data["s3_parent_upload_path"], model_name=self.json_data["model"])

            for f in train_json_path:
                self.logger.debug("### Files in train_json_path :{}".format(f))

        else:
            # If specifying your own model exports, make sure you have a file named "training_params" that specifies batch_siize, RGB mean and img_ht and img_width
            model_exports = self.json_data["pipeline"]["model_committee_files_path"]
            train_json_path = []

            for path in model_exports:
                files= os.listdir(path)
                json_fname = [x for x in files if "training_params" in x][0]
                train_json_path.append(os.path.join(path, json_fname))


        self.logger.info("### Using models for AL :{}".format(model_exports))

        num_samples = self.json_data["pipeline"].get("samples", 2000)

        # Create location for storing csv files
        # .../imageset_builder/ActiveLearning/results/active_learning_pipeline_<model_name>/csv_files_flagged
        csv_base_loc = os.path.join(self.results_dir_path, 'csv_files_flagged')
        self.create_dirs(csv_base_loc)

        # csv_file holding top K(default=20k) scoring images from AL. Basically all entries from all cameras and customerss grouped together in one place
        all_csv_file = os.path.join(csv_base_loc, 'consolidated_AL_scores.csv')
        write_headers = True

        start_date, end_date = self.get_start_end_date(self.json_data["pipeline"]["date_start"], self.json_data["pipeline"]["date_end"])

        # Path through Committee of models
        ### Import necessary module ###
        if self.json_data['type'] == 'Classification':
            if self.selection_strategy == 'QBC':
                active_learning_model_obj = ActiveLearningClassification(model_dir=model_exports)
            elif self.selection_strategy == 'uncertainty_sampling':
                raise ValueError('{} Selection Strategy for {} is not yet implemented'.format(self.selection_strategy, self.model_type)) 
            else:
                raise ValueError('{} Selection Strategy for {} is not yet implemented'.format(self.selection_strategy, self.model_type)) 
        elif self.json_data['type'] == 'ObjectDetection':
            if self.selection_strategy == 'QBC':
                raise ValueError('{} Selection Strategy for {} is not yet implemented'.format(self.selection_strategy, self.model_type)) 
            elif self.selection_strategy == 'uncertainty_sampling':
                active_learning_model_obj = ActiveLearningObjectDetection(model_dir=model_exports[0], batch_size=16) # 16 is goood for 600x600 on a k80
            else:
                raise ValueError('{} Selection Strategy for {} is not yet implemented'.format(self.selection_strategy, self.model_type)) 
        else:
            raise ValueError('{} Active Learning is not yet implemented'.format(self.json_data['type']))

        QC_WB_obj = QCWBInteract()

        # Folder of images scored by AL
        AL_scored_images_path = os.path.join(self.results_dir_path, 'scored_images')

        # Per customer
        for customer_dict in self.json_data["pipeline"]['customers']:
            customer_name = customer_dict['name']
            bucket, key = AWSHelperClass.extract_bucket_and_key(customer_dict['img_s3_base_path'])
            self.logger.info("bucket:{}, key:{}".format(bucket, key))
            env = bucket.split('-')[1]

            # Create S3 object
            src_s3_obj = AWSHelperClass.create_s3_obj(environment=env)

            # Get QC Workbench accurate data
            self.logger.info("customer_dict['node_name'] = {}".format(customer_dict['nodename']))

            # Per node/camera
            for node in customer_dict['nodename']:

                ######### Get QC workbench accurate/ inaccurate data ########

                QC_dict = {True: 'accurate', False: 'inaccurate'}

                QC_exclude_imgs = []
                # self.logger.info("Exclude imgs init:{}".format(QC_exclude_imgs))

                # QC WB query for accurate and inaccurate
                for is_accurate, prefix in QC_dict.items():

                    # Folder where images marked accurate/ inaccurate in QC WB are stored
                    # QCWB imgs folder name : .../imageset_builder/ActiveLearning/results/active_learning_pipeline_<model_name>/QCWB_accurate_imgs &
                    # .../imageset_builder/ActiveLearning/results/active_learning_pipeline_<model_name>/QCWB_inaccurate_imgs
                    QCWB_imgs_path = os.path.join(self.results_dir_path, "QCWB_" + prefix + "_imgs")

                    # .../imageset_builder/ActiveLearning/results/active_learning_pipeline_<model_name>/csv_files_flagged/<camera_name>
                    csv_node_loc = os.path.join(csv_base_loc, node)
                    self.create_dirs(csv_node_loc)
                    QC_WB_csv = os.path.join(csv_node_loc, node + "_QCWB_" + prefix + ".csv")

                    # Get database entries
                    s3_files_list = QC_WB_obj.get_entries_db(sql_query_path=self.sql_path,
                                                                    customer=customer_name,
                                                                    node=node, start_date=start_date.strftime("%Y-%m-%d %H:%M:%S"),
                                                                    end_date=end_date.strftime("%Y-%m-%d %H:%M:%S"),
                                                                    is_accurate=is_accurate, model_name=self.json_data["model"])
                    print(s3_files_list)

                    # Generate CSV for the files
                    QC_WB_obj.download_images_gen_csv(s3_files_list, QC_WB_csv, QCWB_imgs_path, src_s3_obj)

                    # These files should be excluded when doing AL
                    QC_exclude_imgs.extend(list(map(lambda x: os.path.basename(x), s3_files_list)))

                self.logger.debug("### QC excluded images : {}".format(QC_exclude_imgs))

                ########################################################################

                local_path = os.path.join(AL_scored_images_path, bucket)
                self.create_dirs(local_path)

                # Download S3 images per day
                step = timedelta(days=1)
                temp = start_date
                while start_date <= end_date:
                    key_node = key.format(node, start_date.strftime("%Y"), start_date.strftime("%m"), start_date.strftime("%d"))
                    self.logger.debug("bucket:{}, key:{}".format(bucket, key_node))
                    AWSHelperClass.download_folder_from_s3(src_s3_obj, bucket, key_node, dest_folder_path=local_path, file_type='.jpg')
                    start_date += step
                start_date = temp

                end_id = key_node.find(node)+len(node)
                img_dir = os.path.join(local_path, key_node[:end_id])  # key_node = image_readings/db8cam03_lehighhanson_prd/video0/atlftpd/2019/08/21/00"
                self.logger.info("### Running AL on img dir : {}".format(img_dir))
                AL_scores_csv_path = os.path.join(csv_node_loc, node+'_scores.csv')

		### AL_image_set = img_dir - QC_exclude_imgs - annotated_data_set

                # Run AL excluding files obtained from QC WB
                scores_csv, scores_csv_header = active_learning_model_obj.run_main(img_dir, AL_scores_csv_path, s3_bucket_key=bucket, exclude_imgs=QC_exclude_imgs)
                

                # Write to the consolidated csv file also
                if len(scores_csv):
                    with open(all_csv_file, 'a') as all_fh:
                        if write_headers:
                            all_fh.write(scores_csv_header)
                            write_headers = False
                        for row in scores_csv:
                            all_fh.write(row)


            self.logger.info("### Images from AL located at : {}".format(AL_scored_images_path))
            self.logger.info("### Images from QC WB located at : {}".format(QCWB_imgs_path))
            self.logger.info("### CSV files located at : {}".format(csv_base_loc))

        # rank and take the top_k
        img_scores = pd.read_csv(all_csv_file)
        img_scores = img_scores.sort_values(by=['score'], ascending=False)
        img_scores = img_scores.iloc[:self.json_data['pipeline']['top_k']]
        img_scores.to_csv(all_csv_file, sep=',', index=False) # no longer actually all the images. Only the TOP_K

        # Sample a subset of images/or all images flagged
        subsampled_csv_path = self.create_subsampled_csv(all_csv_file, num_samples)


        if not self.json_data["pipeline"]["model_committee_files_path"]:
            # Upload csv files to S3 whose models were used in Model Committee
            s3_path =  os.path.join(self.json_data["s3_parent_upload_path"], self.json_data["model"], version)
            s3_bucket, s3_key = AWSHelperClass.extract_bucket_and_key(s3_path)
            env = s3_bucket.split('-')[1]
            s3_obj = AWSHelperClass.create_s3_obj(env)
            AWSHelperClass.upload_folder_to_s3(s3_obj, s3_bucket=s3_bucket, s3_file_key=s3_key, folder_path=csv_base_loc)

        # If there is an S3 path specified for uploading for annotation, do it here
        if self.json_data["pipeline"]["annotation_s3_upload_path"]:
            index_file_path = self.create_index_file(subsampled_csv_path, self.json_data["pipeline"]["annotation_s3_upload_path"])
            self.upload_for_annotation(self.json_data["pipeline"]["annotation_s3_upload_path"], subsampled_csv_path, index_file_path)

        self.logger.info("######### DONE ########")

    def create_subsampled_csv(self, csv_path, num_samples):
        """
        Function to sample "num_samples" rows from csv in csv_path
        :param csv_path: Full path to csv path containing all images flagged
        :param num_samples: Num of samples
        :return csv with num samples rows
        """
        # function to subsample images
        fldr, fname = os.path.split(csv_path)

        subsampled_csv = os.path.join(fldr, "subsampled_{}_{}.csv".format(fname.split('.')[0], num_samples))

        df = pd.read_csv(csv_path)
        df = shuffle(df)

        if df.shape[0]:
            # Sample only when entries are present

            # If no of samples more than no of images flagged, sample all flagged
            if df.shape[0] >= num_samples:
                df = df.sample(n=num_samples, replace=False)
            else:
                df = df.sample(n=df.shape[0], replace=False)

            df.to_csv(subsampled_csv, sep=',', index=False)

        return subsampled_csv

    def create_index_file(self, csv_file_path, task_base_s3_folder):
        """
        Function to create index file used by CloudFactory
        :param csv_file_path: Path to csv file
        :param task_base_s3_folder: The S3 folder where images are being uploaded.
        Basically ["pipeline"]["annotation_s3_upload_path"] in config
        :return Location to index file
        """

        
        fname = task_base_s3_folder.split('/')[-1]
        json_dict = {}
        json_dict["task_base_s3_folder"] = task_base_s3_folder

        json_dict["media_file_dict"] = {}

        with open(csv_file_path, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)

            for row in csv_reader:
                img_name = os.path.basename(row["s3_link"])

                out = {"orig_s3_url": row["s3_link"], "img_step_id": None}
                json_dict["media_file_dict"][img_name] = out

        index_file_path = os.path.join(self.results_dir_path, fname+'.json')

        with open(index_file_path, 'w') as json_fh:
            json.dump(json_dict, json_fh)

        return index_file_path

    def upload_for_annotation(self, s3_path, csv_file_path, index_file_path):
        """
        Function to upload index file and images for annotation
        :param s3_path: S3 path to upload to
        :param csv_file_path: Csv file path containing rows with imaage local path and S3 path generated during pipeline
        :param index_file_path: Index json file path
        :return:
        """

        s3_bucket, s3_key = AWSHelperClass.extract_bucket_and_key(s3_path)
        env = s3_bucket.split('-')[1]

        s3_obj = AWSHelperClass.create_s3_obj(env)

        with open(csv_file_path, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)

            for row in csv_reader:
                fname = os.path.basename((row["img_local_path"]))
                s3_file_key = os.path.join(s3_key, fname)
                AWSHelperClass.upload_file(s3_obj, s3_bucket=s3_bucket, s3_file_key=s3_file_key, file_path=row["img_local_path"])


        index_fname = os.path.basename(index_file_path)
        s3_file_key = os.path.join(s3_key, index_fname)
        AWSHelperClass.upload_file(s3_obj, s3_bucket=s3_bucket, s3_file_key=s3_file_key, file_path=index_file_path)

        self.logger.info("### Uploaded files to S3 -> {} for annotation".format(s3_path))


def get_config_file_path(user_json_path):
    """
    Function to get config file path based on users input at command line
    :param user_json_path: Json path given by user
    :return:
    """

    # Get dir .../imageset_builder/ActiveLearning
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    # Get dir .../imageset_builder/ActiveLearning/configs
    config_dir = os.path.join(base_dir, "configs")
    if not os.path.exists(config_dir):
        os.makedirs(config_dir)

    # Get type of path
    type_path = get_type_of_path(user_json_path, file_type=".json")

    if type_path == "Unknown":
        raise Exception("### Something wrong with input config file path ###")

    elif type_path == "local_folder":
        raise Exception("### Input to pipeline should be path to a json config file. Not folder ###")

    elif type_path == "s3_file":
        print("### Got config file as s3_file location. Downloading ###")
        # Download config file
        s3_bucket, s3_key = AWSHelperClass.extract_bucket_and_key(user_json_path)
        s3_obj = AWSHelperClass.create_s3_obj(s3_bucket.split('-')[1])
        fname = os.path.basename(user_json_path)
        config_path = os.path.join(config_dir, fname)
        AWSHelperClass.download_file(s3_obj, s3_bucket, s3_key, config_path)

    elif type_path == "local_file":
        print("#### Got config file as local location ####")
        config_path = user_json_path

    elif type_path == "s3_folder":
        # if input is s3 folder, it should point to higher level ACtive Learning folder where the models versions are stored
        # Ex: s3://....../<model_name>. From here, we get the latest version and
        # download the config file


        # get latest config file
        # Get latest version number from S3
        s3_bucket, s3_key = AWSHelperClass.extract_bucket_and_key(user_json_path)
        s3_obj = AWSHelperClass.create_s3_obj(s3_bucket.split('-')[1])

        try:
            folders = AWSHelperClass.get_folders_s3(s3_obj, s3_bucket, s3_key)
            latest_version = max(list(map(lambda x: re.findall(r'v(\d+)', x), folders)))
            latest_version_no = int(latest_version[0])
            print("##### Got config file location as an S3 folder. "
                  "Assuming this is the parent level folder for the model for Active Learning. "
                  "The script will now proceed to download config file based on the latest version :v{} of Active Learning run ####".format(latest_version_no))

        except Exception as e:
            raise Exception(
                " ### Something wrong with getting previous version. The folders obtained are :{} ###".format(folders))

        # Get config file "v<no>_config_.....json from prev version
        s3_prev_folder_key = os.path.join(s3_key, "v{}".format(latest_version_no))
        file_list = AWSHelperClass.get_match_file_from_s3(s3_obj, s3_bucket, s3_prev_folder_key,
                                                          match_str="v{}_config".format(latest_version_no))
        fname = os.path.basename(file_list[0])
        config_path = os.path.join(config_dir, fname)
        AWSHelperClass.download_file(s3_obj, s3_bucket, file_list[0], config_path)

    return config_path

def parse_args():
    parser = argparse.ArgumentParser(add_help=True)

    parser.add_argument(
        "--json_config_path",
        type=str,
        required=True,
        help="Full path to JSON config file",
    )
    parser.add_argument(
        "--logging_level",
        type=int,
        required=False,
        default=20,
        help="logging level. DEBUG:10, INFO: 20",
    )

    parsed_args = parser.parse_args()

    return parsed_args


# store the origin signal handler so it can be restored by our graceful signal handler
__ORIGINAL_SIGINT_HANDLER = None

def exit_gracefully(signum, frame):
    # restore the original signal handler as otherwise evil things will happen
    # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
    signal.signal(signal.SIGINT, __ORIGINAL_SIGINT_HANDLER)

    try:
        if input("\nAre you sure you want to quit?? (y/n)> ").lower().startswith('y'):
            sys.exit(1)

    except KeyboardInterrupt:
        print("Exiting")
        sys.exit(1)

    # restore the exit gracefully handler here    
    signal.signal(signal.SIGINT, exit_gracefully)

if __name__ == "__main__":
    # store the original SIGINT handler
    __ORIGINAL_SIGINT_HANDLER = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)

    # with Config file
    args = parse_args()
    config_path = get_config_file_path(args.json_config_path)
    pipeline_obj = ActiveLearningPipeline(config_path, args.logging_level)
    pipeline_obj.run()