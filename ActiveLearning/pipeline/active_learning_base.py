from abc import ABC, abstractmethod


class ActiveLearningBaseClass(ABC):
    # Base class for Active Learning
    def __init__(self):
        """
        Abstract base class for active learning problems

        """
        super().__init__()

    @abstractmethod
    def score(self):
        """
        abstract method which defines image scoring method

        """
        pass

    def write_scores_to_csv(self, csv_loc, img_scores, s3_bucket_key=None):
        """
        Function to write conflicts to csv file
        :param csv_loc: Location of csv file to be written
        :param img_scores: a list of img_locations and corresponding scores 
        :param s3_bucket_key: S3 bucket and key the images belong to
        """
        # Csv file with columns : "img_local_path", "s3_link", "committee_inference"
        rows_list = []
        with open(csv_loc, 'w') as f:
            if s3_bucket_key:
                header = 'img_local_path,s3_link,score\n'
            else:
                header = 'img_local_path,score\n'
            f.write(header)
            for img_loc, score in img_scores:
                if s3_bucket_key:
                    # Since images during pipeline save structure of S3, look for bucket in img path and generate S3 path
                    id = img_loc.find(s3_bucket_key)
                    s3_link = "s3://" + img_loc[id:]
                    row_str = "{}, {}, {}\n".format(img_loc, s3_link, score)
                else:
                    row_str = "{}, {}\n".format(img_loc, score)
                f.write(row_str)
                rows_list.append(row_str)
        return rows_list, header

    def run_main(self, img_dir, csv_loc, exclude_imgs=None, s3_bucket_key=None):
        """
        run the selection process over a directory of images. Be sure to exclude flagged images (for example QC flagged it). Results are dumped to a csv.

        :param img_dir : directory to run inference against 
        :param csv_loc : location to save the csv
        :param exclude_imgs : list of image names to exlude
        :param s3_bucket_key : s3 bucket and key the images belong to 
        """
        if self.selection_strategy == 'QBC':
            preds = []
            for M_i in self.M:
                preds_i, filepaths = M_i.infer_directory(img_dir, exclude_imgs)
                preds.append(preds_i)
        else:
            preds, filepaths = self.M.infer_directory(img_dir, exclude_imgs) 
        img_scores = self.score(preds, filepaths) 
        rows_list, header = self.write_scores_to_csv(csv_loc, img_scores, s3_bucket_key)

        return rows_list, header
