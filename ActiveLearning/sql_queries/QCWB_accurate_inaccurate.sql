SELECT
     i.images[1]
FROM image_step i
INNER JOIN image_step_annotation ia
        ON i.collection_time=ia.collection_time
        AND i.camera_id=ia.camera_id
        AND i.gateway_id=ia.gateway_id
        AND i.step_name IN ('atlftpd', 'default', 'motion')
WHERE ((%(is_accurate)s IS TRUE AND ia.is_accurate) OR (%(is_accurate)s IS FALSE AND not ia.is_accurate))
AND i.collection_time BETWEEN %(start_date)s AND %(end_date)s
AND i.images[1] LIKE %(node)s
AND ia.step_name = %(model_name)s
ORDER by i.collection_time asc