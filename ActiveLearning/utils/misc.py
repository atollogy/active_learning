import os


def get_type_of_path(full_path, file_type):
    """
    Function to determine if the full path is a path to a S3 file or S3 folder or local file or local folder
    :param full_path: Full location path
    :param file_type: string specifying file type. Ex: ".json"
    :return: One of "s3_file", "s3_folder", "local_file", "local_folder", "Unknown"
    """

    if full_path.startswith("s3://") and full_path.endswith(file_type):
        out = "s3_file"
    elif full_path.startswith("s3://") and not full_path.endswith(file_type):
        out = "s3_folder"
    elif not full_path.startswith("s3://") and full_path.endswith(file_type) and os.path.isfile(full_path):
        out = "local_file"
    elif not full_path.startswith("s3://") and not full_path.endswith(file_type) and not os.path.isfile(file_type):
        out = "local_folder"
    else:
        out = "unknown"

    return out