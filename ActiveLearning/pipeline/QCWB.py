import sqlalchemy
import pandas as pd
import os
from utils.aws_helper import AWSHelperClass
import csv


class QCWBInteract:
    """
    Class and methods to interact with QC workbench
    """
    prdDB_hostname = "prd-v040-cluster.cluster-ro-ctbthluuwetz.us-west-2.rds.amazonaws.com"
    user_id = "atollogy"
    password = "atollogy"
    port = 5432
    server = "prod"

    def get_entries_db(self, sql_query_path, customer, node, start_date, end_date, is_accurate, model_name):
        """
        Function to get database entries for a sql query
        :param sql_query_path: Full path to SQL query whose results from db are needed. The SQL query should return image links in S3
        :param customer: Customer name
        :param node: Camera/ gateway
        :param start_date: Start date(UTC)
        :param end_date: End date (UTC)
        :param is_accurate: If True, get entries Qc'ed as Accurate else Inaccurate
        :param model_name: Name of model whose QC results are needed
        :return: List of image paths in S3
        """
        print("### QCWB startdate:{}".format(start_date))
        print("### QCWB end_date : {}".format(end_date))

        if '-' in customer:
            customer = customer.split('-')[2]

        ### Fetch Data from DB
        query = open(sql_query_path).read()
        try:
            engine = sqlalchemy.create_engine(
                "postgresql+psycopg2://"
                + self.user_id
                + ":"
                + self.password
                + "@"
                + self.prdDB_hostname
                + "/"
                + customer
            )

            # print("DB query for {}, {} - is_accurate:{}, model: {}".format(customer, node, is_accurate, model_name))
            data = pd.read_sql(
                query,
                engine,
                params={"node": '%'+node+'%',
                        'start_date': start_date,
                        'end_date': end_date,
                        'is_accurate': is_accurate,
                        'model_name': model_name},
            )

            return data.images.tolist()
        except Exception as err:
            print(f'QCWB access failed with error {err}')

        return []

    def download_images_gen_csv(self, s3_files_list, QCWB_csv_loc, QCWB_imgs_path, src_s3_obj=None):
        """
        :param s3_files_list: data from get_entries_db which is a list of S3 links
        :param csv_loc: complete path to CSV file to be written
        :param QCWB_imgs_path: Complete folder path to download images to
        :param src_s3_obj: S3 object for "prd" env
        :return:
        """
        # Get QC workbench data
        if s3_files_list:
            if src_s3_obj is None:
                src_s3_obj = AWSHelperClass.create_s3_obj('prd')

            with open(QCWB_csv_loc, 'w') as f:
                col_names = ["img_local_path", "s3_link"]
                writer = csv.DictWriter(f, fieldnames=col_names)
                writer.writeheader()

                for s3_file in s3_files_list:
                    bucket, key = AWSHelperClass.extract_bucket_and_key(s3_file)
                    QC_img_path = os.path.join(QCWB_imgs_path, bucket, key)
                    AWSHelperClass.download_file(src_s3_obj, bucket, key, dest_file_path=QC_img_path)
                    writer.writerow({"img_local_path": QC_img_path, "s3_link": s3_file})


if __name__ =="__main__":
    db_obj = QCWBInteract()
    data = db_obj.get_entries_db('/home/ubuntu/ActiveLearning/sql_queries/QCWB_accurate_inaccurate.sql',
                    'lehighhanson',
                    'db8cam17_lehighhanson_prd',
                    '2020-05-08 01:00:00',
                    '2020-05-22 23:59:59', True, 'saferack_person')
    print(data)