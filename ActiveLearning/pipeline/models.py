import tensorflow as tf
from abc import ABC, abstractmethod
import os
import numpy as np
import cv2
import time

class ModelBase(ABC):
    def __init__(self):
        super().__init__()

    def infer_directory(self, img_dir, exclude_imgs=None):
        """
        :param img_dir: location pointing to the local images directory you want to run inference on
        """
        img_batch = []
        file_names = []
        responses = {key_i: [] for key_i in self.response_keys}
        for root, dirnames, fnames in os.walk(img_dir):
            for i, fname in enumerate(fnames):
                if fname.endswith('.jpg') and fname not in exclude_imgs:
                    image_loc_i = os.path.join(root, fname)
                    if os.path.getsize(image_loc_i) == 0: # skip empty images
                        print('skipping {} b/c filesize is 0'.format(image_loc_i))
                        continue

                    img_i = cv2.imread(image_loc_i, 1)
                    if img_i is None:
                        print('skipping {} b/c fail to read image(None)'.format(image_loc_i))
                        continue

                    img_i = img_i[:, :, [2, 1, 0]]  # convert to RGB
                    img_i = cv2.resize(img_i, self.img_dim)

                    # keep track of np images and the original file loc
                    img_batch.append(img_i)
                    file_names.append(image_loc_i)

                    if len(img_batch) == self.batch_size:
                        img_batch = np.stack(img_batch, axis=0)
                        s = time.time()
                        batch_res = self.predict_batch(img_batch)
                        responses = self.update_responses(responses, batch_res)
                        f = time.time()
                        print('{}/{} mb-fps:{}'.format(i, len(fnames), self.batch_size/float(f-s)))
                        img_batch = []

        # run remainders that can't fit in a full batch size
        if len(img_batch) > 0:
            n_pad = self.batch_size - len(img_batch)
            pad = np.zeros([n_pad] + list(img_batch[0].shape))
            img_batch = np.stack(img_batch, axis=0)
            img_batch = np.concatenate([img_batch, pad], axis=0)
            batch_res = self.predict_batch(img_batch)
            responses = self.update_responses(responses, batch_res, n_pad)

        responses = {k: np.array(v) for k,v in responses.items()} 
        return responses, file_names

    def update_responses(self, responses, batch_response, n_pad=0):
        for key_i in responses.keys():
            if n_pad > 0:
                responses[key_i].extend(batch_response[key_i][:-n_pad])
            else:
                responses[key_i].extend(batch_response[key_i])
        return responses

    @abstractmethod
    def predict_batch(self, np_imgs):
        pass

class Model(ModelBase):
    def __init__(self, model_dir, batch_size, img_dim):
        '''
        Parameters
        ----------
        model_dir : directory of the SavedModel
        batch_size : number of images in a minibatch
        img_dim : dimensions to resize images to 
        '''
        self.batch_size = batch_size
        self.img_dim = img_dim 
        self.model_dir = model_dir
        self.savedmodel = tf.saved_model.load(self.model_dir)
        self.predict_fn = self.savedmodel.signatures['serving_default']
        super().__init__()

    @abstractmethod
    def predict_batch(self, np_imgs):
        pass

class ObjectDetection(Model):
    def __init__(self, model_dir, batch_size=32, img_dim=(600,600)):
        '''
        Parameters
        ----------
        model_dir : directory of the SavedModel
        batch_size : number of images in a minibatch
        img_dim : dimensions to resize images to 
        '''
        self.response_keys = ['detection_classes', 'detection_boxes', 'detection_scores', 'num_detections']
        super().__init__(model_dir, batch_size, img_dim)

    def filter_response(self, detection_scores, detection_classes, detection_boxes, num_detections):
        return detection_scores[:num_detections], detection_classes[:num_detections], detection_boxes[:num_detections]

    def predict_batch(self, np_imgs):
        '''
        Parameters
        ----------
        np_imgs: 32 images in a numpy array
     
        Returns
        -------
        out : dictionary, but it's shape depends on the model
        '''
        out = self.predict_fn(tf.convert_to_tensor(np_imgs, tf.uint8))
        return out

class Classification(Model):
    def __init__(self, model_dir, batch_size=32, img_dim=(224, 224)):
        '''
        Parameters
        ----------
        model_dir : directory of the SavedModel
        batch_size : number of images in a minibatch
        img_dim : dimensions to resize images to 
        '''
        self.response_keys = ['classes', 'probabilities']
        super().__init__(model_dir, batch_size, img_dim)

    def predict_batch(self, np_imgs):
        '''
        Parameters
        ----------
        np_imgs: 32 images in a numpy array
     
        Returns
        -------
        out : dictionary, but it's shape depends on the model
        '''
        out = self.predict_fn(tf.convert_to_tensor(np_imgs, tf.float32))
        return out
