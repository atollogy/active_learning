# 1) Active Learning

## Set up

Clone the required repositories

Current active branches: 

* activelearning: "master"

```bash

# Clone the Active Learning repository
git clone https://bitbucket.org/atollogy/activelearning.git 
cd activelearning
```

(optionally use [nvidia-docker](https://github.com/NVIDIA/nvidia-docker)) Build the container
```
docker image build -t al:1.0 -f docker/Dockerfile.AL .
```
Run the container
```
docker container run --gpus all --detach -it --name al_demo al:1.0
```
Attach to the container
```
docker attach al_demo
```

Add to pythonpath

```bash
cd ActiveLearning
export PYTHONPATH=`pwd`:$PYTHONPATH 
```

Create an aws credentials file at ```~/.aws/credentials```
```
[default]
aws_access_key_id=xxxxxxxx
aws_secret_access_key=xxxxxxxx

[dev]
aws_access_key_id=xxxxxxxxx
aws_secret_access_key=xxxxxxxx

[prd]
aws_access_key_id=xxxxxxxxx
aws_secret_access_key=xxxxxxxxxxxxxx
```

## Start pipeline to flag images with Active Learning:

Start flagging images using the script _**"ActiveLearning/pipeline/active_learning_pipeline.py"**_

The script can be triggered by passing **_ONE_** of the following inputs

* Passing S3 location of config json

    ```
    python pipeline/active_learning_pipeline.py --json_config_path=s3://...../config.json
    ```


* Passing parent level folder location of S3 for a particular Active Learning model. 
In this case, the config file generated during the last run of training is downloaded and used
    
    ```
    python pipeline/active_learning_pipeline.py --json_config_path=s3://atl-dev--zoo/.ml/training/ActiveLearningTest/cargo_status
    ```

  
* Passing local file location of config
    
    ```
    python pipeline/active_learning_pipeline.py --json_config_path=/.../....../blah.json
    ```
    
## Config
The Active Learning related config parameters can be found in the [Config Parameters Confluence Page](https://atollogy.atlassian.net/wiki/spaces/CV/pages/706805881/Active+Learning+-+Configuration) under "model", "version", "type", "s3_parent_upload_path", and "pipeline" keys.

If you would like to locally test using Active Learning for image selection without uploading files to s3, set "annotation_s3_upload_path" to null.

It's important to consider which "selection_strategy" is available for a problem "type". Currently the uncertainty_sampling selection strategy is only available for type ObjectDetection and the QBC selection strategy is only available for type Classification.
  
# 2) Folders in repository

1. **activelearning/configs**: Contains json config samples

2. **activelearning/pipeline**: Contains files related to Active Learning
    * _models.py_: file containing classes which manage loading and inference of SavedModels for classification and object detection
    * _active_learning_base.py_: Base class for Active Learning
    * _classification.py_: Class for Flagging images specifically for Classification - Currently it only supports QBC
    * _object_detection.py_: Class for Flagging images specifically for Classification - Currently it only supports QBC
    * _active_learning_pipeline.py_: Class and methods for flagging images using Active Learning. Generic module
    * _QCWB.py_: Class and methods to interact with QC workbench and gather images marked accurate/inaccurate by human QC user

3. **activelearning/preprocess_dataset**: Contains files related to any preprocessing on a given dataset. Note that all files in this module should be named "preprocess_<model_name>" and should have a function named "convert_dataset" 
which takes a single argument i.e path to the json folder containing annotation jsons and returns new folder path contained processed json annotations

4. **activelearning/sql_queries**: Contains sql files to be used in any of the scripts

5. **activelearning/train_models**: Contains files related to training of committee of models
    * _active_learning_training_pipeline.py_: Main script to handle training of models, generic
    * _active_learning_training_Classification_: Script for training of ResNet Classification model
    
6. **activelearning/utils**: Contains utility files
    * _aws_helper.py_: Class to interact with S3
    * _create_tf_records.py_: Script to create TF records
    * _misc.py_: Contains miscellaneous function utilities
    * _read_TFRecord.py_: File to read TF records
    
# 3) Notes

1. Result directory for training:  _".../activelearning/training_results_<modelname>_<version_str>"_

2. Result directory for QBC: _".../activelearning/results/active_learning_pipeline_<model_name>"_

3. Configs if auto-generated are saved at : _".../activelearning/configs/generated_<version str>_<model_name>.json"_

4. All files are uploaded to S3 : s3_parent_upload_path": _"<s3_parent_upload_path in config>/<model_name>/<version_str>"_
