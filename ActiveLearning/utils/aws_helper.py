import boto3
from botocore import config
import os
import datetime
import logging

import functools
import time


class AWSHelperClass():

    @staticmethod
    def extract_bucket_and_key(s3_path):
        """
        Function to return the bucket and key when s3 path is given
        :param s3_path: Full s3 path Ex: "s3://atl-prd-cemexusa-rawdata/image_readings/db8cam01_cemexusa_prd/video0/atlftpd/2019/09/21/00"
        :return bucket : atl-prd-cemexusa-rawdata
        :return key: image_readings/db8cam01_cemexusa_prd/video0/atlftpd/2019/09/21/00
        """
        s3_split = s3_path.split('/')
        bucket = s3_split[2]
        key = '/'.join(s3_split[3:])
        return bucket, key


    @staticmethod
    def create_s3_obj(environment, retries=200):

        creds_file_loc = os.path.join(os.environ['HOME'], '.aws', 'credentials')
        if os.path.exists(creds_file_loc):
            session = boto3.Session(profile_name=environment)
            s3_obj = session.client(
                's3', 
                config=config.Config(
                    retries = {
                        # AWS caps retry time at 20s so 200 should cover ~an hour of disconnect
                        'max_attempts': retries,
                    }
                )
            )
            return s3_obj
        else:
            raise FileNotFoundError('Missing AWS Credentials file: "{}" '.format(creds_file_loc))


    @staticmethod
    def download_file(s3_obj, s3_bucket, s3_file_key, dest_file_path):
        """
        Function to download a single file from S3. Retains same hierarchy
        :param s3_obj: s3 client object
        :param s3_bucket: Name of bucket Ex: atl-prd-trimac-rawdata
        :param s3_file_key: S3 key to file Ex: image_readings/db8cam01_trimac_prd/video0/atlftpd/2019/08/21/xxxx.jpg
        :param dest_file_path: Full path to the folder where the images should be saved
        :return:
        """

        print("Downloading file :{}/{} -> {}".format(s3_bucket, s3_file_key, dest_file_path))
        fldr, file_name = os.path.split(dest_file_path)
        if not os.path.exists(fldr):
            os.makedirs(fldr)

        s3_obj.download_file(s3_bucket, s3_file_key, dest_file_path)


    @staticmethod
    def download_folder_from_s3(s3_obj, s3_bucket, s3_folder_key, dest_folder_path, file_type=None, save_structure=True, exclude_str='thumbnail'):
        """
        Function to download an entire folder of images stored in S3 to a given local path. Retains same hierarchy
        :param s3_obj: s3 client object
        :param s3_bucket: Name of bucket Ex: atl-prd-trimac-rawdata
        :param s3_folder_key: S3 key to folder Ex: image_readings/db8cam01_trimac_prd/video0/atlftpd/2019/08/21/
        :param dest_folder_path: Full path to the folder where the images should be saved
        :return:
        """
        num_files_downloaded = 0
        IsTruncated = True
        next_token = ''
        while IsTruncated:
            if next_token:
                resp = s3_obj.list_objects_v2(Bucket=s3_bucket, Prefix=s3_folder_key,
                   ContinuationToken=next_token)
            else:
                resp = s3_obj.list_objects_v2(Bucket=s3_bucket, Prefix=s3_folder_key)

            IsTruncated = resp["IsTruncated"]
            next_token = resp.get('NextContinuationToken')

            try:
                file_list = list(map(lambda x: x["Key"], resp["Contents"]))
                # print("file_list :{}".format(file_list))
            except KeyError as err:
                print('Encountered KeyError ({}) for {}'.format(err, s3_bucket))
                continue

            for i, s3_file_key in enumerate(file_list):
                if not s3_file_key.endswith('/'):
                    # Only files
                    if save_structure:
                        local_file_path = os.path.join(dest_folder_path, s3_file_key)
                    else:
                        fname = s3_file_key.split('/')[-1]
                        # print("fname:{}".format(fname))
                        local_file_path = os.path.join(dest_folder_path, fname)
                    download_file = (((not file_type) or (file_type in s3_file_key)) and 
                        ((not exclude_str) or (exclude_str not in s3_file_key)))
                    if download_file:
                        try:
                            AWSHelperClass.download_file(s3_obj, s3_bucket, s3_file_key, local_file_path)
                            num_files_downloaded += 1
                        except Exception as e:
                            raise e

        return num_files_downloaded

    @staticmethod
    def copy_file(s3_obj, src, dst_folder):
        """
        Function to copy a file from src to dst in S3
        :param s3_obj: s3 client object
        :param src: source s3 path Ex: s3://dummy-bucket/dummy-file
        :param dst_folder: destination s3 folder Ex: s3://dummy-bucket/dummy-folder/
        :return: Copies file from src to dst
        """
        src_bucket, src_key = AWSHelperClass.extract_bucket_and_key(src)
        
        copy_src = {
            'Bucket': src_bucket,
            'Key': src_key
        }
        
        dst_bucket, dst_key = AWSHelperClass.extract_bucket_and_key(dst_folder)
        
        fname = src.split('/')[-1]
        
        dst_key = os.path.join(dst_key,fname)
        
        s3_obj.copy(
            copy_src,
            Bucket=dst_bucket,
            Key=dst_key
        )

    @staticmethod
    def upload_file(s3_obj, s3_bucket, s3_file_key, file_path):
        """
        Function to upload a single file to S3
        :param s3_obj: s3 client object
        :param s3_bucket: Name of bucket Ex: atl-dev--zoo
        :param s3_file_key: s3_folder_key: dest location of file in S3: Ex: testupload/testfile.jpeg
        :param file_path: Full path to file to be uploaded Ex: /home/testfile.jpeg
        :return: Uploads file : s3://atl-dev--zoo/testupload/testupload/testfile.jpeg
        """
        s3_obj.upload_file(file_path, s3_bucket, s3_file_key)
        print(" ### Uploading file :{} -> {}".format(file_path, s3_file_key))

    @staticmethod
    def upload_folder_to_s3(s3_obj, s3_bucket, s3_file_key, folder_path):
        """
        Function to upload entire folder to S3
        :param s3_obj: s3 client object
        :param s3_bucket: Name of bucket Ex: atl-dev--zoo
        :param s3_file_key:  S3 key  Ex: testupload/
        :param folder_path: Path to folder to be uploaded Ex: /home/testfolder
        :return: Uploads folder to s3://atl-dev--zoo/testupload/testfolder
        """

        base_folder_name = os.path.basename(folder_path)
        par_dir_name = os.path.dirname(folder_path)

        for root, dirs, files in os.walk(folder_path):
            for file in files:
                sub_level = os.path.join(root[len(par_dir_name)+1:], file)  #+1 to get rid of "/", due to os.path.join behaviour
                dest_bucket_key = os.path.join(s3_file_key, sub_level)

                AWSHelperClass.upload_file(s3_obj, s3_bucket, dest_bucket_key, os.path.join(root, file))

    @staticmethod
    def get_folders_s3(s3_obj, s3_bucket, s3_folder_key):
        """
        Function to return all folders(not files) with the level <s3_bucket>/<s3_folder_key>/<folder_name>.
        If there are no folders or the s3_folder_key is invalid, empty list of returned
        :param s3_obj: s3 client object
        :param s3_bucket: Name of bucket Ex: atl-dev--zoo
        :param s3_folder_key:  S3 key  Ex: testupload/
        :return folders: Returns all(and only at this level) Folders(not files) in the path <s3_bucket>/<s3_folder_key>/<folder_name>
        folders is a list of s3_keys for folders: [<s3_folder_key>/<folder_name_1>, <s3_folder_key>/<folder_name_2>]
        """
        IsTruncated = True
        next_token = ''

        # Have to make sure there is a "/" at the end of s3_key. Otherwise lower level folders arent listed
        if not s3_folder_key.endswith('/'):
            s3_folder_key = s3_folder_key+"/"

        folders = []

        while IsTruncated:
            if next_token:
                resp = s3_obj.list_objects_v2(Bucket=s3_bucket, Prefix=s3_folder_key,
                                              ContinuationToken=next_token, Delimiter='/')

            else:
                resp = s3_obj.list_objects_v2(Bucket=s3_bucket, Prefix=s3_folder_key, Delimiter='/')

            IsTruncated = resp["IsTruncated"]
            next_token = resp.get('NextContinuationToken')

            common_prefixes = resp.get("CommonPrefixes", None)

            if common_prefixes:
                out = [x.get("Prefix") for x in resp.get("CommonPrefixes")]
                folders.extend(out)
                # print("Subfolders")
                for i in out:
                    print(i)

        return folders

    @staticmethod
    def get_match_file_from_s3(s3_obj, s3_bucket, s3_folder_key, match_str=None):
        """
        Function to get S3 key for files which have the file_prefix
        :param s3_obj: s3 client object
        :param s3_bucket: Name of bucket Ex: atl-dev--zoo
        :param s3_folder_key:  S3 key  Ex: testupload/   -> Folder under which to look at
        :param match_str: Look for all files which contain this matching string. Ex: match_str = "model.ckpt". Look for all files under s3_bucket/s3_folder_key for files/folders which contain model_ckpt
        :return file_list: List of S3 keys for all matching files
        """

        IsTruncated = True
        next_token = ''
        file_list= []
        while IsTruncated:
            if next_token:
                resp = s3_obj.list_objects_v2(Bucket=s3_bucket, Prefix=s3_folder_key,
                                              ContinuationToken=next_token)
            else:
                resp = s3_obj.list_objects_v2(Bucket=s3_bucket, Prefix=s3_folder_key)

            IsTruncated = resp["IsTruncated"]
            next_token = resp.get('NextContinuationToken')

            try:
                for x in resp["Contents"]:
                    if match_str in x["Key"][len(s3_folder_key):]:
                        file_list.append(x["Key"])

            except KeyError as err:
                print('Encountered KeyError ({}) for {}'.format(err, s3_bucket))
                continue

        return file_list


if __name__ == "__main__":
    s3_obj = AWSHelperClass.create_s3_obj('dev')
    s3_bucket, s3_key = AWSHelperClass.extract_bucket_and_key(
        "s3://atl-dev--zoo/.ml/training/ActiveLearning/LP_readable_non_readable/v1/TF_records")
    file_list = AWSHelperClass.get_match_file_from_s3(s3_obj, s3_bucket, s3_key, match_str=".pbtxt")
    print(file_list)
